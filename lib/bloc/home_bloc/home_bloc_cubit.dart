import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/movie.dart';
import 'package:majootestcase/services/api_service.dart';

part 'home_bloc_state.dart';

class HomeBlocCubit extends Cubit<HomeBlocState> {
  HomeBlocCubit() : super(HomeBlocInitialState());

  void fetchingData() async {
    emit(HomeBlocLoadingState());
    Movie? response = await ApiServices().getMovieList("iron man");
    if (response == null) {
      emit(
        HomeBlocErrorState(
          "Terjadi kesalahan, Silahkan periksa koneksi internet anda",
        ),
      );
      print(response.toString());
    } else {
      print(response.results.toString());
      emit(HomeBlocLoadedState(response.results!.toList()));
    }
  }
}
