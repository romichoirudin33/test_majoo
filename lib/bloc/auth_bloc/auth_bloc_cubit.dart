import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/services/database_helper.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit() : super(AuthBlocInitialState());

  void fetchHistoryLogin() async {
    emit(AuthBlocInitialState());
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool? isLoggedIn = sharedPreferences.getBool("is_logged_in");
    if (isLoggedIn == null) {
      emit(AuthBlocLoginState());
    } else {
      if (isLoggedIn) {
        emit(AuthBlocLoggedInState());
      } else {
        emit(AuthBlocLoginState());
      }
    }
  }

  void loginUser(User user, BuildContext context) async {
    int? auth = await DatabaseHelper.instance.authenticate(user);
    if (auth == null || auth == 0) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('Login gagal, periksa kembali inputan anda'),
        ),
      );
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('Login Berhasil'),
        ),
      );
      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();
      emit(AuthBlocLoadingState());
      await sharedPreferences.setBool("is_logged_in", true);
      String data = user.toJson().toString();
      sharedPreferences.setString("user_value", data);
      emit(AuthBlocLoggedInState());
    }
  }

  void registerPage() async {
    emit(AuthBlocRegisterState());
  }

  void doRegister(User user, BuildContext context) async {
    int? auth = await DatabaseHelper.instance.checkEmail(user);
    if (auth != null) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('Email ini telah terdaftar di database'),
        ),
      );
    } else {
      int create = await DatabaseHelper.instance.createUser(user);
      print(create.toString());
      if (create > 0) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text('Registrasi berhasil ...'),
          ),
        );
        SharedPreferences sharedPreferences =
            await SharedPreferences.getInstance();
        emit(AuthBlocLoadingState());
        await sharedPreferences.setBool("is_logged_in", true);
        String data = user.toJson().toString();
        sharedPreferences.setString("user_value", data);
        emit(AuthBlocLoggedInState());
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text('Terjadi kesalahan, ulangi kembali ...'),
          ),
        );
      }
    }
  }

  void changeLogin() async {
    emit(AuthBlocLoginState());
  }

  void backLogin() async {
    emit(AuthBlocLoginState());
  }

  void logout() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    await sharedPreferences.setBool("is_logged_in", false);
    emit(AuthBlocLoginState());
  }
}
