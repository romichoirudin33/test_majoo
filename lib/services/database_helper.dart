import 'package:majootestcase/models/user.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHelper {
  DatabaseHelper._privateConstructor();
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

  static Database? _database;
  Future<Database> get database async => _database ?? await _initDatabase();

  Future<Database> _initDatabase() async {
    var databasesPath = await getDatabasesPath();
    String path = join(databasesPath, 'demo.db');
    return await openDatabase(path, version: 1, onCreate: _onCreate);
  }

  Future _onCreate(Database db, int version) async {
    await db.execute(
        'CREATE TABLE users (id INTEGER PRIMARY KEY, username TEXT, email TEXT, password TEXT)');
  }

  Future<int?> authenticate(User user) async {
    Database db = await instance.database;
    String? email = user.email;
    String? password = user.password;
    int? users = Sqflite.firstIntValue(
      await db.rawQuery(
          'select * from users where email = "$email" and password = "$password"'),
    );
    return users;
  }

  Future<int?> checkEmail(User user) async {
    Database db = await instance.database;
    String? email = user.email;
    int? users = Sqflite.firstIntValue(
      await db.rawQuery('select * from users where email = "$email"'),
    );
    return users;
  }

  Future<int> createUser(User user) async {
    Database db = await instance.database;
    return await db.insert('users', user.toMap());
  }
}
