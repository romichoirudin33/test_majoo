import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:majootestcase/models/movie.dart';
import 'package:majootestcase/services/dio_config_service.dart' as dioConfig;

class ApiServices {
  Future<Movie?> getMovieList(String q) async {
    try {
      var dio = await dioConfig.dio();
      Response<String> response = await dio!.get("title/find?q=$q");
      Movie? movie = Movie.fromJson(jsonDecode(response.data!));
      return movie;
    } catch (e) {
      print(e.toString());
      return null;
    }
  }
}
