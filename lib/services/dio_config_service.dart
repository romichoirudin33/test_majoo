import 'dart:async';
import 'package:dio/dio.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

Dio? dioInstance;
createInstance() async {
  var options = BaseOptions(
      baseUrl: "https://imdb8.p.rapidapi.com/",
      connectTimeout: 12000,
      receiveTimeout: 12000,
      headers: {
        "x-rapidapi-key": "77822ecc35mshc35d90d2707eeffp15a9cejsnf90c373dae73",
        "x-rapidapi-host": "imdb8.p.rapidapi.com"
      });
  dioInstance = new Dio(options);
  dioInstance?.interceptors.add(
    PrettyDioLogger(
        requestHeader: true,
        requestBody: true,
        responseBody: true,
        responseHeader: false,
        error: true,
        compact: true,
        maxWidth: 90),
  );
}

Future<Dio?> dio() async {
  await createInstance();
  dioInstance?.options.baseUrl = "https://imdb8.p.rapidapi.com/";
  return dioInstance;
}
