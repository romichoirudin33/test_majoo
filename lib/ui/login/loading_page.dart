// ignore_for_file: implementation_imports

import 'package:flutter/material.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:provider/src/provider.dart';

class LoadingLoginPage extends StatelessWidget {
  const LoadingLoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Column(
            children: [
              SizedBox(
                height: 40,
                width: 40,
                child: CircularProgressIndicator(),
              ),
              Text('loading'),
              TextButton(
                onPressed: () {
                  context.read<AuthBlocCubit>().backLogin();
                },
                child: Text('loading page'),
              )
            ],
          ),
        ),
      ),
    );
  }
}
