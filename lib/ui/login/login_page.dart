import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/ui/home/home_page.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginPage> {
  final _emailController = TextController();
  final _passwordController = TextController();
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  bool _isObscurePassword = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<AuthBlocCubit, AuthBlocState>(
        listener: (context, state) {
          if (state is AuthBlocLoggedInState) {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (_) => BlocProvider(
                  create: (context) => HomeBlocCubit()..fetchingData(),
                  child: HomePage(),
                ),
              ),
            );
          }
        },
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(top: 75, left: 25, bottom: 25, right: 25),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                _titlePage(),
                _form(context),
                _register(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _titlePage() {
    return Container(
      margin: EdgeInsets.only(bottom: 15),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Selamat Datang',
            style: TextStyle(
              fontSize: 24,
              fontWeight: FontWeight.bold,
            ),
          ),
          Text(
            'Silahkan login terlebih dahulu',
            style: TextStyle(
              fontSize: 15,
              fontWeight: FontWeight.w400,
            ),
          ),
        ],
      ),
    );
  }

  Widget _form(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 50, bottom: 25),
      child: Form(
        key: formKey,
        child: Column(
          children: [
            CustomTextFormField(
              controller: _emailController,
              isEmail: true,
              hint: 'Example@123.com',
              label: 'Email',
              validator: (val) {
                final pattern =
                    new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
                if (val != null)
                  return pattern.hasMatch(val)
                      ? null
                      : 'Masukkan e-mail yang valid';
              },
              context: context,
            ),
            CustomTextFormField(
              context: context,
              label: 'Password',
              hint: 'password',
              controller: _passwordController,
              isObscureText: _isObscurePassword,
              suffixIcon: IconButton(
                icon: Icon(
                  _isObscurePassword
                      ? Icons.visibility_off_outlined
                      : Icons.visibility_outlined,
                ),
                onPressed: () {
                  setState(() {
                    this._isObscurePassword = !this._isObscurePassword;
                  });
                },
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 40),
              width: MediaQuery.of(context).size.width,
              height: 40,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(6),
                  ),
                ),
                onPressed: () {
                  if (formKey.currentState?.validate() == true) {
                    User user = User(
                      email: _emailController.value,
                      password: _passwordController.value,
                    );
                    context.read<AuthBlocCubit>().loginUser(user, context);
                  } else {
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        content: Text(
                          'Form tidak boleh kosong, mohon cek kembali data yang anda inputkan',
                        ),
                      ),
                    );
                  }
                },
                child: Text('Login'),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _register() {
    return Container(
      child: Align(
        alignment: Alignment.center,
        child: TextButton(
          onPressed: () async {
            context.read<AuthBlocCubit>().registerPage();
          },
          child: RichText(
            text: TextSpan(
              text: 'Belum punya akun? ',
              style: TextStyle(color: Colors.black45),
              children: [
                TextSpan(
                  text: 'Daftar',
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
