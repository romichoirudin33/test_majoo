import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/models/user.dart';

class RegisterPage extends StatefulWidget {
  RegisterPage({Key? key}) : super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final _userameController = TextController();
  final _emailController = TextController();
  final _passwordController = TextController();
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  bool _isObscurePassword = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(
              top: 75,
              left: 25,
              bottom: 25,
              right: 25,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _titlePage(),
                _formPage(context),
                _bottomPage(context),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _titlePage() {
    return Container(
      margin: EdgeInsets.only(bottom: 15),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Register',
            style: TextStyle(
              fontSize: 24,
              fontWeight: FontWeight.bold,
            ),
          ),
          Text(
            'Silahkan isi data berikut untuk pendaftaran',
            style: TextStyle(
              fontSize: 15,
              fontWeight: FontWeight.w400,
            ),
          )
        ],
      ),
    );
  }

  Widget _formPage(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 50, bottom: 25),
      child: Form(
        key: formKey,
        child: Column(
          children: [
            CustomTextFormField(
              context: context,
              controller: _userameController,
              hint: 'example',
              label: 'Username',
            ),
            CustomTextFormField(
              context: context,
              controller: _emailController,
              isEmail: true,
              hint: 'Example@123.com',
              label: 'Email',
              validator: (val) {
                final pattern =
                    new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
                if (val != null)
                  return pattern.hasMatch(val)
                      ? null
                      : 'Masukkan e-mail yang valid';
              },
            ),
            CustomTextFormField(
              context: context,
              label: 'Password',
              hint: 'password',
              controller: _passwordController,
              isObscureText: _isObscurePassword,
              suffixIcon: IconButton(
                icon: Icon(
                  _isObscurePassword
                      ? Icons.visibility_off_outlined
                      : Icons.visibility_outlined,
                ),
                onPressed: () {
                  setState(() {
                    this._isObscurePassword = !this._isObscurePassword;
                  });
                },
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 40),
              width: MediaQuery.of(context).size.width,
              height: 40,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(6),
                  ),
                ),
                onPressed: () {
                  if (formKey.currentState?.validate() == true) {
                    User user = User(
                      username: _userameController.value,
                      email: _emailController.value,
                      password: _passwordController.value,
                    );
                    context.read<AuthBlocCubit>().doRegister(user, context);
                  } else {
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        content: Text(
                          'Form tidak boleh kosong, mohon cek kembali data yang anda inputkan',
                        ),
                      ),
                    );
                  }
                },
                child: Text('Register'),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _bottomPage(BuildContext context) {
    return Container(
      child: Align(
        alignment: Alignment.center,
        child: TextButton(
          onPressed: () async {
            context.read<AuthBlocCubit>().changeLogin();
          },
          child: RichText(
            text: TextSpan(
              text: 'Sudah punya akun? ',
              style: TextStyle(color: Colors.black45),
              children: [
                TextSpan(
                  text: 'Login',
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
