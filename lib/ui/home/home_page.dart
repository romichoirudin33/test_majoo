import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/ui/extra/error_screen.dart';
import 'package:majootestcase/ui/home/home_loaded_page.dart';
import 'package:majootestcase/ui/home/home_loading_page.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Beranda'),
        actions: [
          IconButton(
            onPressed: () {
              context.read<HomeBlocCubit>().fetchingData();
            },
            icon: Icon(Icons.refresh_sharp),
          ),
          IconButton(
            onPressed: () {
              context.read<AuthBlocCubit>().logout();
            },
            icon: Icon(Icons.logout),
          ),
        ],
      ),
      body: BlocBuilder<HomeBlocCubit, HomeBlocState>(
        builder: (context, state) {
          if (state is HomeBlocLoadedState) {
            return HomeLoadedPage(result: state.data);
          } else if (state is HomeBlocLoadingState) {
            return HomeLoadingPage();
          } else if (state is HomeBlocInitialState) {
            return HomeLoadingPage();
          } else if (state is HomeBlocErrorState) {
            return ErrorScreen(
              message: state.error,
              retry: () {
                context.read<HomeBlocCubit>().fetchingData();
              },
            );
          }
          return Center(
            child: HomeLoadingPage(),
          );
        },
      ),
    );
  }
}
