import 'package:flutter/material.dart';
import 'package:majootestcase/common/widget/loading.dart';

class HomeLoadingPage extends StatelessWidget {
  const HomeLoadingPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: 4,
      itemBuilder: (context, index) {
        return loadingItemWidget(context);
      },
    );
  }

  Widget loadingItemWidget(BuildContext context) {
    double witdh = MediaQuery.of(context).size.width;
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(10.0),
        ),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            flex: 1,
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10.0),
                  bottomLeft: Radius.circular(10.0),
                ),
              ),
              child: LoadingShimmer(width: witdh, height: 150),
            ),
          ),
          Expanded(
            flex: 3,
            child: Container(
              padding: const EdgeInsets.symmetric(
                vertical: 10.0,
                horizontal: 10.0,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    margin: EdgeInsets.only(bottom: 15.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        LoadingShimmer(
                          width: MediaQuery.of(context).size.width,
                          height: 20,
                        ),
                        LoadingShimmer(
                          width: MediaQuery.of(context).size.width,
                          height: 12,
                        ),
                      ],
                    ),
                  ),
                  LoadingShimmer(
                    width: MediaQuery.of(context).size.width / 2,
                    height: 12,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
