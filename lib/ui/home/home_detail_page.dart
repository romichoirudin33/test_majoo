import 'package:flutter/material.dart';
import 'package:majootestcase/models/movie.dart' as Movie;

class HomeDetailPage extends StatelessWidget {
  const HomeDetailPage({Key? key, required this.movie}) : super(key: key);

  final Movie.Result movie;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Detail Movie'),
      ),
      body: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            flex: 1,
            child: ClipRRect(
              child: Image.network(
                  movie.image?.url ?? 'https://i.imgur.com/sjDBHUW.jpg'),
            ),
          ),
          Expanded(
            flex: 3,
            child: Container(
              padding:
                  const EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    margin: EdgeInsets.only(bottom: 15.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          movie.title ?? movie.legacyNameText ?? '-',
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.w700),
                        ),
                        Text(movie.titleType ?? 'actor'),
                      ],
                    ),
                  ),
                  Text(
                    movie.year == null ? '' : movie.year.toString(),
                    style: TextStyle(fontSize: 12, color: Colors.grey),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
