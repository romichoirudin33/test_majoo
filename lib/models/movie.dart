// To parse this JSON data, do
//
//     final movie = movieFromJson(jsonString);

import 'dart:convert';

Movie movieFromJson(String str) => Movie.fromJson(json.decode(str));

String movieToJson(Movie data) => json.encode(data.toJson());

class Movie {
  Movie({
    this.query,
    this.results,
    this.types,
  });

  String? query;
  List<Result>? results;
  List<String>? types;

  factory Movie.fromJson(Map<String, dynamic> json) => Movie(
        query: json["query"],
        results:
            List<Result>.from(json["results"].map((x) => Result.fromJson(x))),
        types: List<String>.from(json["types"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "query": query,
        "results": results != null
            ? List<dynamic>.from(results!.map((x) => x.toJson()))
            : null,
        "types":
            types != null ? List<dynamic>.from(types!.map((x) => x)) : null,
      };
}

class Result {
  Result({
    this.id,
    this.image,
    this.runningTimeInMinutes,
    this.title,
    this.titleType,
    this.year,
    this.legacyNameText,
    this.name,
  });

  String? id;
  Image? image;
  int? runningTimeInMinutes;
  String? title;
  String? titleType;
  int? year;
  String? legacyNameText;
  String? name;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        image: json["image"] == null ? null : Image.fromJson(json["image"]),
        runningTimeInMinutes: json["runningTimeInMinutes"] == null
            ? null
            : json["runningTimeInMinutes"],
        title: json["title"] == null ? null : json["title"],
        titleType: json["titleType"] == null ? null : json["titleType"],
        year: json["year"] == null ? null : json["year"],
        legacyNameText:
            json["legacyNameText"] == null ? null : json["legacyNameText"],
        name: json["name"] == null ? null : json["name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "image": image == null ? null : image!.toJson(),
        "runningTimeInMinutes":
            runningTimeInMinutes == null ? null : runningTimeInMinutes,
        "title": title == null ? null : title,
        "titleType": titleType == null ? null : titleType,
        "year": year == null ? null : year,
        "legacyNameText": legacyNameText == null ? null : legacyNameText,
        "name": name == null ? null : name,
      };
}

class Image {
  Image({
    this.height,
    this.id,
    this.url,
    this.width,
  });

  int? height;
  int? width;
  String? id;
  String? url;

  factory Image.fromJson(Map<String, dynamic> json) => Image(
        height: json["height"],
        id: json["id"],
        url: json["url"],
        width: json["width"],
      );

  Map<String, dynamic> toJson() => {
        "height": height,
        "id": id,
        "url": url,
        "width": width,
      };
}
