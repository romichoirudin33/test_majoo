class User {
  final String? email;
  final String? username;
  final String? password;

  User({this.email, this.username, this.password});

  User.fromJson(Map<String, dynamic> json)
      : email = json['email'],
        password = json['password'],
        username = json['username'];

  Map<String, dynamic> toJson() =>
      {'email': email, 'password': password, 'username': username};

  factory User.fromMap(Map<String, dynamic> json) => User(
        username: json["username"],
        email: json["email"],
        password: json["password"],
      );

  Map<String, dynamic> toMap() => {
        "username": username,
        "email": email,
        "password": password,
      };
}
