# majootestcase

Project test case for Mobile Engineer

## Getting Started

For testing the apps you can to install app-debug.apk

This project use

- [flutter_bloc](https://pub.dev/packages/flutter_bloc)
- [sqflite](https://pub.dev/packages/sqflite)
- [shared_preferences](https://pub.dev/packages/shared_preferences)
- [dio](https://pub.dev/packages/dio)
- [shimmer](https://pub.dev/packages/shimmer)
- [cached_network_image](https://pub.dev/packages/cached_network_image)

For help information this project please call me at

[email](https://mailto:romichoirudin33@gmail.com)
[wa](https://wa.me/6285790544333)
